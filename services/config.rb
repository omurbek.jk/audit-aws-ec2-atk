coreo_aws_rule "ec2-get-all-instances-older-than" do
  action :define
  service :ec2
  link "http://kb.cloudcoreo.com/mydoc_ec2-alert-to-kill.html"
  display_name "Alert to Kill"
  description "EC2 instance was launched within the last ${MINUTES_AGO} minutes that violates tag policy (does not have the necessary tags)."
  category "Policy"
  suggested_action "Review instance tags and terminate the instance if it does not comply to tagging policy."
  level "Low"
  objectives ["instances"]
  audit_objects ["object.reservation_set.instances_set.launch_time"]
  operators ["<"]
  raise_when ["${MINUTES_AGO}.minutes.ago"]
  id_map "object.reservation_set.instances_set.instance_id"
end

coreo_uni_util_variables "atk-planwide" do
  action :set
  variables([
                {'COMPOSITE::coreo_uni_util_variables.atk-planwide.composite_name' => 'PLAN::stack_name'},
                {'COMPOSITE::coreo_uni_util_variables.atk-planwide.plan_name' => 'PLAN::name'},
                {'COMPOSITE::coreo_uni_util_variables.atk-planwide.results' => 'unset'},
                {'COMPOSITE::coreo_uni_util_variables.atk-planwide.number_violations' => '0'}
            ])
end

coreo_aws_rule_runner_ec2 "advise-ec2-atk" do
  rules ${AUDIT_AWS_EC2_ATK_ALERT_LIST}
  action :run
  regions ${AUDIT_AWS_EC2_ATK_REGIONS}
end

coreo_uni_util_variables "atk-update-planwide-1" do
  action :set
  variables([
                {'COMPOSITE::coreo_uni_util_variables.atk-planwide.results' => 'COMPOSITE::coreo_aws_rule_runner_ec2.advise-ec2-atk.report'},
                {'COMPOSITE::coreo_uni_util_variables.atk-planwide.number_violations' => 'COMPOSITE::coreo_aws_rule_runner_ec2.advise-ec2-atk.number_violations'},

            ])
end

coreo_uni_util_jsrunner "sort-json-for-expected-tags" do
  action :run
  data_type "json"
  provide_composite_access true
  json_input '{ "composite name":"PLAN::stack_name",
                "plan name":"PLAN::name",
                "violations": COMPOSITE::coreo_aws_rule_runner_ec2.advise-ec2-atk.report}'
  function <<-EOH

const EXPECTED_TAGS = [${AUDIT_AWS_EC2_ATK_EXPECTED_TAGS}];
const EC2_LOGIC_LENGTH = "${AUDIT_AWS_EC2_ATK_TAG_LOGIC}" === "or" ? 1 : EXPECTED_TAGS.length;

const violations = json_input.violations;

let counterForCloudObjects = 0;
let counterForViolations = 0;
const regionKeys = Object.keys(violations);
regionKeys.forEach(regionKey => {
    Object.keys(violations[regionKey]).forEach(violationKey => {
        const similarNumber = getSimilarNumber(violations[regionKey][violationKey].tags, EXPECTED_TAGS)
        Object.keys(violations[regionKey][violationKey].violations).forEach(alertKey => {
            if (similarNumber >= EC2_LOGIC_LENGTH) {
                delete violations[regionKey][violationKey]['violations'][alertKey];
                counterForViolations--;
                if (Object.keys(violations[regionKey][violationKey]['violations']).length === 0) {
                    delete violations[regionKey][violationKey];
                }
            }
            counterForCloudObjects++;
            counterForViolations++;
        });
    });
});

function getSimilarNumber(tags, EXPECTED_TAGS) {
    if (typeof tags === 'undefined') return 0;
    return EXPECTED_TAGS.reduce((expectedAcc, EXPECTED_TAG) => {
        return expectedAcc + tags.reduce((tagAcc, tagElem) => {
                tagElem = tagElem.tag || tagElem;
                const hasExpected = tagElem['key'].toLowerCase() === EXPECTED_TAG.toLowerCase();
                return hasExpected ? ++tagAcc : tagAcc;
            }, 0);
    }, 0);
}



coreoExport('counterForCloudObjects', JSON.stringify(counterForCloudObjects));
coreoExport('counterForViolations', JSON.stringify(counterForViolations));
callback(violations);
  EOH
end

coreo_uni_util_variables "aws-update-planwide-2" do
  action :set
  variables([
                {'COMPOSITE::coreo_aws_rule_runner_ec2.advise-ec2-atk.report' => 'COMPOSITE::coreo_uni_util_jsrunner.sort-json-for-expected-tags.return'},
                {'GLOBAL::number_violations' => 'COMPOSITE::coreo_uni_util_jsrunner.sort-json-for-expected-tags.counterForViolations'},
                {'GLOBAL::number_cloud_objects' => 'COMPOSITE::coreo_uni_util_jsrunner.sort-json-for-expected-tags.counterForCloudObjects'}
            ])
end

coreo_uni_util_jsrunner "tags-to-notifiers-array-ec2-atk" do
  action :run
  data_type "json"
  provide_composite_access true
  packages([
               {
                   :name => "cloudcoreo-jsrunner-commons",
                   :version => "1.10.7-beta64"
               },
               {
                   :name => "js-yaml",
                   :version => "3.7.0"
               }      ])
  json_input '{ "compositeName":"PLAN::stack_name",
                "planName":"PLAN::name",
                "teamName":"PLAN::team_name",
                "cloudAccountName": "PLAN::cloud_account_name",
                "violations": COMPOSITE::coreo_aws_rule_runner_ec2.advise-ec2-atk.report}'
  function <<-EOH

const compositeName = json_input.compositeName;
const planName = json_input.planName;
const cloudAccount = json_input.cloudAccountName;
const cloudObjects = json_input.violations;
const teamName = json_input.teamName;

const NO_OWNER_EMAIL = "${AUDIT_AWS_EC2_ATK_RECIPIENT}";
const OWNER_TAG = "${AUDIT_AWS_EC2_ATK_OWNER_TAG}";
const ALLOW_EMPTY = "${AUDIT_AWS_EC2_ATK_ALLOW_EMPTY}";
const SEND_ON = "${AUDIT_AWS_EC2_ATK_SEND_ON}";
const htmlReportSubject = "${HTML_REPORT_SUBJECT}";

const alertListArray = ${AUDIT_AWS_EC2_ATK_ALERT_LIST};
const ruleInputs = {};

let userSuppression;
let userSchemes;

const fs = require('fs');
const yaml = require('js-yaml');
function setSuppression() {
  try {
      userSuppression = yaml.safeLoad(fs.readFileSync('./suppression.yaml', 'utf8'));
  } catch (e) {
    if (e.name==="YAMLException") {
      throw new Error("Syntax error in suppression.yaml file. "+ e.message);
    }
    else{
      console.log(e.name);
      console.log(e.message);
      userSuppression=[];
    }
  }

  coreoExport('suppression', JSON.stringify(userSuppression));
}

function setTable() {
  try {
    userSchemes = yaml.safeLoad(fs.readFileSync('./table.yaml', 'utf8'));
  } catch (e) {
    if (e.name==="YAMLException") {
      throw new Error("Syntax error in table.yaml file. "+ e.message);
    }
    else{
      console.log(e.name);
      console.log(e.message);
      userSchemes={};
    }
  }

  coreoExport('table', JSON.stringify(userSchemes));
}
setSuppression();
setTable();

const argForConfig = {
    NO_OWNER_EMAIL, cloudObjects, userSuppression, OWNER_TAG,
    userSchemes, alertListArray, ruleInputs, ALLOW_EMPTY,
    SEND_ON, cloudAccount, compositeName, planName, htmlReportSubject, teamName
}


function createConfig(argForConfig) {
    let JSON_INPUT = {
        compositeName: argForConfig.compositeName,
        htmlReportSubject: argForConfig.htmlReportSubject,
        planName: argForConfig.planName,
        teamName: argForConfig.teamName,
        violations: argForConfig.cloudObjects,
        userSchemes: argForConfig.userSchemes,
        userSuppression: argForConfig.userSuppression,
        alertList: argForConfig.alertListArray,
        disabled: argForConfig.ruleInputs,
        cloudAccount: argForConfig.cloudAccount
    };
    let SETTINGS = {
        NO_OWNER_EMAIL: argForConfig.NO_OWNER_EMAIL,
        OWNER_TAG: argForConfig.OWNER_TAG,
        ALLOW_EMPTY: argForConfig.ALLOW_EMPTY, SEND_ON: argForConfig.SEND_ON,
        SHOWN_NOT_SORTED_VIOLATIONS_COUNTER: false
    };
    return {JSON_INPUT, SETTINGS};
}

const {JSON_INPUT, SETTINGS} = createConfig(argForConfig);
const CloudCoreoJSRunner = require('cloudcoreo-jsrunner-commons');

const emails = CloudCoreoJSRunner.createEmails(JSON_INPUT, SETTINGS);
const suppressionJSON = CloudCoreoJSRunner.createJSONWithSuppress(JSON_INPUT, SETTINGS);

coreoExport('JSONReport', JSON.stringify(suppressionJSON));
coreoExport('report', JSON.stringify(suppressionJSON['violations']));

callback(emails);
  EOH
end

coreo_uni_util_jsrunner "tags-rollup-atk" do
  action :run
  data_type "text"
  json_input 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-ec2-atk.return'
  function <<-EOH

const notifiers = json_input;

function setTextRollup() {
    let emailText = '';
    let numberOfViolations = 0;
    let usedEmails=new Map();
    notifiers.forEach(notifier => {
        const hasEmail = notifier['endpoint']['to'].length;
        const email = notifier['endpoint']['to'];
        if(hasEmail && usedEmails.get(email)!==true) {
            usedEmails.set(email,true);
            numberOfViolations += parseInt(notifier['numberOfViolatingCloudObjects']);
            emailText += "recipient: " + notifier['endpoint']['to'] + " - " + "Violations: " + notifier['numberOfViolatingCloudObjects'] + ", Cloud Objects: "+ (notifier["num_violations"]-notifier['numberOfViolatingCloudObjects']) + "\\n";
        }
    });

    textRollup += 'Total Number of matching Cloud Objects: ' + numberOfViolations + "\\n";
    textRollup += 'Rollup' + "\\n";
    textRollup += emailText;

    coreoExport('number_violations', numberOfViolations);
}


let textRollup = '';
setTextRollup();

callback(textRollup);
  EOH
end

coreo_uni_util_notify "advise-ec2-atk-to-tag-values" do
  action((("${AUDIT_AWS_EC2_ATK_RECIPIENT}".length > 0)) ? :notify : :nothing)
  notifiers 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-ec2-atk.return'
end

coreo_uni_util_notify "advise-atk-rollup" do
  action((("${AUDIT_AWS_EC2_ATK_RECIPIENT}".length > 0) and (! "${AUDIT_AWS_EC2_ATK_OWNER_TAG}".eql?("NOT_A_TAG"))) ? :notify : :nothing)
  type 'email'
  allow_empty ${AUDIT_AWS_EC2_ATK_ALLOW_EMPTY}
  send_on "${AUDIT_AWS_EC2_ATK_SEND_ON}"
  payload '
composite name: PLAN::stack_name
plan name: PLAN::name
COMPOSITE::coreo_uni_util_jsrunner.tags-rollup-atk.return
  '
  payload_type 'text'
  endpoint ({
      :to => '${AUDIT_AWS_EC2_ATK_RECIPIENT}', :subject => 'CloudCoreo ec2 rule results on PLAN::stack_name :: PLAN::name'
  })
end

coreo_uni_util_jsrunner "tags-to-notifiers-array-kill-scripts" do
  action :run
  data_type "text"
  provide_composite_access true
  json_input '{ "number_violations":"GLOBAL::number_violations",
                "number_cloud_objects":"GLOBAL::number_cloud_objects",
                "violations": COMPOSITE::coreo_aws_rule_runner_ec2.advise-ec2-atk.report}'
  function <<-EOH

const number_violations  = json_input.number_violations;
const number_cloud_objects = json_input.number_cloud_objects;

let HTMLKillScripts = `<div style="background: #000;
                            color: white;
                            word-break:break-all;
                            padding: 20px;
                            line-height: 21px;">`;

HTMLKillScripts+= '<p style="color: white;">#!/bin/bash</p>' +
    '<p style="color: white;"># number of cloud objects: ' + number_cloud_objects + '</p>' +
    '<p style="color: white;"># number of violations: ' + number_violations + '</p>';

const violations = json_input.violations;
Object.keys(violations).forEach(region => {
    Object.keys(violations[region]).forEach(cloudObj => {
        const ruleKeys = Object.keys(violations[region][cloudObj]);
        ruleKeys.forEach(rule => {
            HTMLKillScripts+= "<p style='color: white;'>aws ec2 terminate-instances --instance-ids " + cloudObj + "instance --region "+
            region + '</p>';
        });
    });
});

HTMLKillScripts+= '</div>';

callback(HTMLKillScripts);
  EOH
end

coreo_uni_util_variables "atk-update-planwide-3" do
  action :set
  variables([
                {'COMPOSITE::coreo_aws_rule_runner_ec2.advise-ec2-atk.report' => 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-ec2-atk.report'},
                {'COMPOSITE::coreo_uni_util_variables.atk-planwide.results' => 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-ec2-atk.JSONReport'},
                {'GLOBAL::table' => 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-ec2-atk.table'},
                {'GLOBAL::number_violations' => 'COMPOSITE::coreo_uni_util_jsrunner.tags-rollup-atk.number_violations'}
            ])
end

coreo_uni_util_notify "advise-ec2-notify-no-tags-older-than-kill-all-script" do
  action((("${AUDIT_AWS_EC2_ATK_RECIPIENT}".length > 0)) ? :notify : :nothing)
  type 'email'
  allow_empty ${AUDIT_AWS_EC2_ATK_ALLOW_EMPTY}
  send_on "${AUDIT_AWS_EC2_ATK_SEND_ON}"
  payload 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-kill-scripts.return'
  payload_type "html"
  endpoint ({
      :to => '${AUDIT_AWS_EC2_ATK_RECIPIENT}', :subject => 'Untagged EC2 Instances kill script: PLAN::stack_name :: PLAN::name'
  })
end

coreo_uni_util_jsrunner "ec2-atk-remediate-all" do
  action :run
  data_type "json"
  provide_composite_access false
  packages([
               {
                   :name => "aws-sdk",
                   :version => "latest"
               }
           ])
  json_input 'COMPOSITE::coreo_uni_util_variables.atk-planwide.results'
  function <<-EOH

    var AWS = require('aws-sdk');
    var auto_remediate = "${AUDIT_AWS_EC2_ATK_AUTO_REMEDIATE}";

    var result = {};

    var params = {};
    if (auto_remediate === "false") {
        params.DryRun = true;
    } else {
        params.DryRun = false;
    }

    for (var region in json_input["violations"]) {

        result[region] = {};
        var ec2 = new AWS.EC2({region: region});

        for (var inputKey in json_input["violations"][region]) {
            var thisKey = inputKey;

            var instance_id = thisKey;

            params.InstanceIds = [];
            params.InstanceIds.push(instance_id);

            ec2.terminateInstances(params, function (err, data) {
                if (err) {
                    console.log(err, err.stack); // an error occurred
                } else {
                    console.log(data);
                }// successful response
            });

        }
    }

    var rtn = result;
    callback(result);

EOH
end

coreo_aws_s3_policy "cloudcoreo-audit-aws-ec2-atk-policy" do
  action((("${AUDIT_AWS_EC2_ATK_S3_NOTIFICATION_BUCKET_NAME}".length > 0) ) ? :create : :nothing)
  policy_document <<-EOF
{
"Version": "2012-10-17",
"Statement": [
{
"Sid": "",
"Effect": "Allow",
"Principal":
{ "AWS": "*" }
,
"Action": "s3:*",
"Resource": [
"arn:aws:s3:::${AUDIT_AWS_EC2_ATK_S3_NOTIFICATION_BUCKET_NAME}/*",
"arn:aws:s3:::${AUDIT_AWS_EC2_ATK_S3_NOTIFICATION_BUCKET_NAME}"
]
}
]
}
  EOF
end

coreo_aws_s3_bucket "bucket-${AUDIT_AWS_EC2_ATK_S3_NOTIFICATION_BUCKET_NAME}" do
  action((("${AUDIT_AWS_EC2_ATK_S3_NOTIFICATION_BUCKET_NAME}".length > 0) ) ? :create : :nothing)
  bucket_policies ["cloudcoreo-audit-aws-ec2-atk-policy"]
end

coreo_uni_util_notify "cloudcoreo-audit-aws-ec2-atk-s3" do
  action((("${AUDIT_AWS_EC2_ATK_S3_NOTIFICATION_BUCKET_NAME}".length > 0) ) ? :notify : :nothing)
  type 's3'
  allow_empty true
  payload 'COMPOSITE::coreo_uni_util_jsrunner.tags-to-notifiers-array-ec2-atk.report'
  endpoint ({
      object_name: 'aws-ec2-atk-json',
      bucket_name: '${AUDIT_AWS_EC2_ATK_S3_NOTIFICATION_BUCKET_NAME}',
      folder: 'ec2-atk/PLAN::name',
      properties: {}
  })
end
