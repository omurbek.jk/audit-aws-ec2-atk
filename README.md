audit-aws-ec2-atk
============================



## Description
This composite detects which EC2 instances are running which are not "properly tagged" and supports both automatic and scripted remediation


## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/audit-aws-ec2-samples/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

**None**


## Required variables with default

### `AUDIT_AWS_EC2_ATK_ALLOW_EMPTY`:
  * description: Would you like to receive empty reports? Options - true / false. Default is false.
  * default: false

### `AUDIT_AWS_EC2_ATK_SEND_ON`:
  * description: Send reports always or only when there is a change? Options - always / change. Default is change.
  * default: change

### `AUDIT_AWS_EC2_ATK_EXPECTED_TAGS`:
  * description: What tag do you want to see on instances?
  * default: "EXAMPLE_TAG_1", "EXAMPLE_TAG_2"

### `AUDIT_AWS_EC2_ATK_TAG_LOGIC`:
  * description: "or" or "and"
  * default: or

### `AUDIT_AWS_EC2_ATK_REGIONS`:
  * description: List of AWS regions to check. Default is all regions. Choices are us-east-1,us-east-2,us-west-1,us-west-2,ca-central-1,ap-south-1,ap-northeast-2,ap-southeast-1,ap-southeast-2,ap-northeast-1,eu-central-1,eu-west-1,eu-west-1,sa-east-1
  * default: us-east-1, us-east-2, us-west-1, us-west-2, ca-central-1, ap-south-1, ap-northeast-2, ap-southeast-1, ap-southeast-2, ap-northeast-1, eu-central-1, eu-west-1, eu-west-2, sa-east-1

### `AUDIT_AWS_EC2_ATK_AUTO_REMEDIATE`:
  * description: should the plan automatically remediate by terminating not properly tagged instances?
  * default: false

### `MINUTES_AGO`:
  * description: The timeframe (in minutes ago) in which to check for the creation of instances that violate the policy
  * default: 5


## Optional variables with default

### `AUDIT_AWS_EC2_ATK_ALERT_LIST`:
  * description: Which alerts would you like to check for? Default is all ec2 atk alerts. Possible value is ec2-get-all-instances-older-than
  * default: ec2-get-all-instances-older-than

### `AUDIT_AWS_EC2_ATK_OWNER_TAG`:
  * description: Enter an AWS tag whose value is an email address of the owner of the EC2 object. (Optional)
  * default: NOT_A_TAG


## Optional variables with no default

### `HTML_REPORT_SUBJECT`:
  * description: Enter a custom report subject name.

### `AUDIT_AWS_EC2_ATK_RECIPIENT`:
  * description: Enter the email address(es) that will receive notifications. If more than one, separate each with a comma.

### `AUDIT_AWS_EC2_ATK_S3_NOTIFICATION_BUCKET_NAME`:
  * description: Enter S3 bucket name to upload reports. (Optional)

## Tags
1. Operations
1. EC2

## Categories
1. AWS Operations Automation



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/audit-aws-ec2-atk/master/images/diagram.png "diagram")


## Icon
![icon](https://raw.githubusercontent.com/CloudCoreo/audit-aws-ec2-atk/master/images/icon.png "icon")

